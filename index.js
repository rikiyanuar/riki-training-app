/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src/screens/MiniProject1';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => App);
