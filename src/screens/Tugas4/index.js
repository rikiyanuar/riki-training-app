import React, { useState, createContext } from 'react'
import moment from 'moment'
import TodoList from './TodoList'

export const RootContext = createContext()

const Context = () => {
  const [inputValue, setInputValue] = useState("")
  const [list, setList] = useState([])

  const addList = () => {
    if (inputValue.trim() === "") return
    setList([...list, { tgl: moment().format("x"), todo: inputValue }])
    setInputValue("")
  }

  const delList = (tgl) => {
    const newList = list.filter(item => item.tgl !== tgl)
    setList(newList)
  }

  return (
    <RootContext.Provider value={{ inputValue, list, setInputValue, addList, delList }}>
      <TodoList />
    </RootContext.Provider>
  )
}

export default Context