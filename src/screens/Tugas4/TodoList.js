import React, { useContext } from 'react'
import { StyleSheet, StatusBar, SafeAreaView, TextInput, Text, FlatList, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment'
import { RootContext } from './index'

const TodoList = () => {
  const state = useContext(RootContext)

  const ListTile = ({ item }) => {
    return (
      <View style={styles.listTile}>
        <View style={{ flex: 1 }}>
          <Text style={styles.listSubtitle}>{moment(item.tgl, 'x').format("DD/MM/YYYY")}</Text>
          <Text style={styles.listTitle}>{item.todo}</Text>
        </View>
        <TouchableOpacity onPress={() => state.delList(item.tgl)}>
          <Icon color="orangered" size={28} name="trash" />
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <SafeAreaView>
        <Text style={styles.titleText} >Masukkan To-Do List</Text>
        <View style={styles.inputRow}>
          <TextInput
            placeholder="Input here"
            style={styles.textInput}
            onChangeText={inputValue => state.setInputValue(inputValue)}
            value={state.inputValue} />
          <TouchableOpacity onPress={() => state.addList()}>
            <View style={styles.btnPlus}>
              <Icon color="white" size={28} name="add" />
            </View>
          </TouchableOpacity>
        </View>
        <FlatList
          data={state.list}
          renderItem={ListTile}
          keyExtractor={(item) => item.tgl}
        />
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  titleText: {
    marginTop: 18,
    marginHorizontal: 18,
    marginBottom: 6,
    fontSize: 16
  },
  inputRow: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 18,
    marginBottom: 14
  },
  textInput: {
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderWidth: 1,
    borderColor: "rgba(0, 0, 0, 0.6)",
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    flex: 1
  },
  btnPlus: {
    backgroundColor: "rgba(0, 0, 0, 0.6)",
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    padding: 10
  },
  listTile: {
    flexDirection: "row",
    borderTopWidth: 1.5,
    borderTopColor: "#dedede",
    paddingHorizontal: 18,
    paddingVertical: 10
  },
  listTitle: {
    fontSize: 16,
    color: "rgba(0, 0, 0, 0.7)"
  },
  listSubtitle: {
    fontSize: 14,
    color: "rgba(0, 0, 0, 0.6)"
  },
})

export default TodoList