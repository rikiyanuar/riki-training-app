import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

const Tugas1 = () => {
  return (
    <View style={styles.viewport}>
      <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  viewport: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
})

export default Tugas1;