import React from 'react'
import { StyleSheet, ScrollView, StatusBar, SafeAreaView, View, Text, Image } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const profileUri = "https://media-exp1.licdn.com/dms/image/C5103AQEaAOZjWsDC4Q/profile-displayphoto-shrink_200_200/0/1520257686028?e=1613001600&v=beta&t=kuq2G0ZEvZH6a804PYYlgZVqPOL7qrRmGm5zbv4lQTY"

const Tugas2 = () => {
  return (
    <>
      <StatusBar backgroundColor="rebeccapurple" />
      <SafeAreaView style={{ backgroundColor: "#fafafa", flex: 1 }}>
        <ScrollView>
          <AppBar />
          <Profile />
          <Saldo />
          <Pengaturan />
          <Bantuan />
          <Syarat />
          <Keluar />
        </ScrollView>
      </SafeAreaView>
    </>
  )
}

const AppBar = () => {
  return (
    <View style={styles.appBar}>
      <Text style={styles.titleBar}>Account</Text>
    </View>
  )
}

const Profile = () => {
  return (
    <View style={styles.profileRow}>
      <Image style={styles.profilePic} source={{ uri: profileUri }} />
      <Text style={styles.profileText}>Riki Yanuar</Text>
    </View>
  )
}

const Saldo = () => {
  return (
    <View style={styles.saldoRow}>
      <BuildIcons icon="wallet-outline" />
      <Text style={styles.listTextStyle}>Saldo</Text>
      <Text style={styles.saldoText}>Rp 120.000.000</Text>
    </View>
  )
}

const Pengaturan = () => {
  return (
    <View style={styles.listRow}>
      <BuildIcons icon="settings-outline" />
      <Text style={styles.listTextStyle}>Pengaturan</Text>
    </View>
  )
}

const Bantuan = () => {
  return (
    <View style={styles.listRow}>
      <BuildIcons icon="help-circle-outline" />
      <Text style={styles.listTextStyle}>Bantuan</Text>
    </View>
  )
}

const Syarat = () => {
  return (
    <View style={styles.saldoRow}>
      <BuildIcons icon="receipt-outline" />
      <Text style={styles.listTextStyle}>Syarat & Ketentuan</Text>
    </View>
  )
}

const Keluar = () => {
  return (
    <View style={styles.listRow}>
      <BuildIcons icon="log-out-outline" />
      <Text style={styles.listTextStyle}>Keluar</Text>
    </View>
  )
}

const BuildIcons = ({ icon }) => {
  return <Icon color="dimgrey" size={28} style={styles.icon} name={icon} />
}

const styles = StyleSheet.create({
  appBar: {
    height: 54,
    backgroundColor: "rebeccapurple",
    paddingHorizontal: 18,
    justifyContent: "center",
    elevation: 4
  },
  titleBar: {
    fontSize: 22,
    color: "white",
    fontWeight: "bold"
  },
  profileRow: {
    padding: 18,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    marginBottom: 2
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "lightgrey"
  },
  profileText: {
    flex: 1,
    fontSize: 20,
    color: "dimgrey",
    fontWeight: "bold",
    marginLeft: 18
  },
  saldoRow: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    paddingHorizontal: 18,
    paddingVertical: 14,
    marginBottom: 6
  },
  saldoText: {
    fontSize: 16,
    color: "black"
  },
  listRow: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    paddingHorizontal: 18,
    paddingVertical: 14,
    marginBottom: 2
  },
  listTextStyle: {
    flex: 1,
    marginLeft: 18,
    fontSize: 16,
    color: "rgba(0, 0, 0, 0.9)"
  }
})

export default Tugas2