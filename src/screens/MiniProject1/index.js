import React, { useState, createContext } from 'react'
import moment from 'moment'
import Contact from './Contact'

export const RootContext = createContext()

const Context = () => {
  const [inputMode, setInputMode] = useState("list")
  const [inputName, setInputName] = useState("")
  const [inputPhone, setInputPhone] = useState("")
  const [list, setList] = useState([])

  const addList = () => {
    if (inputName.trim() === "" || inputPhone.trim() === "") return
    setList([...list, { tgl: moment().format("x"), name: inputName, phone: inputPhone }])
    setInputMode("list")
    setInputName("")
    setInputPhone("")
  }

  const delList = (tgl) => {
    const newList = list.filter(item => item.tgl !== tgl)
    setList(newList)
  }

  const params = { list, inputMode, setInputMode, inputName, setInputName, inputPhone, setInputPhone, addList, delList }
  return (
    <RootContext.Provider value={params}>
      <Contact />
    </RootContext.Provider>
  )
}

export default Context