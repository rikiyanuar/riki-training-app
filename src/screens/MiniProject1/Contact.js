import React, { useContext } from 'react'
import { StyleSheet, StatusBar, SafeAreaView, TextInput, Text, FlatList, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment'
import { RootContext } from './index'

export default Contact = () => {
  const state = useContext(RootContext)

  const ListTile = ({ item }) => {
    return (
      <View style={styles.listTile}>
        <View style={{ flex: 1 }}>
          <Text style={styles.listTitle}>{item.name}</Text>
          <Text style={styles.listSubtitle}>{item.phone}</Text>
        </View>
        <TouchableOpacity onPress={() => state.delList(item.tgl)}>
          <Icon color="orangered" size={24} name="trash" />
        </TouchableOpacity>
      </View>
    )
  }

  const ListView = () => {
    return (
      <FlatList
        data={state.list}
        renderItem={ListTile}
        keyExtractor={(item) => item.tgl}
      />
    )
  }

  const InputView = () => {
    return (

      <View style={styles.inputColumn}>
        <TextInput
          placeholder="Input Name"
          style={styles.textInput}
          onChangeText={v => state.setInputName(v)}
          value={state.inputName} />
        <TextInput
          keyboardType='phone-pad'
          placeholder="Input Phone Number"
          style={styles.textInput}
          onChangeText={v => state.setInputPhone(v)}
          value={state.inputPhone} />
        <TouchableOpacity onPress={() => state.addList()}>
          <Text style={styles.btnAdd}>SAVE</Text>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <>
      <StatusBar backgroundColor="#FE3284" barStyle="light-content" />
      <SafeAreaView>
        <View style={styles.appBar}>
          <TouchableOpacity onPress={() => state.setInputMode("list")}>
            <Icon color={state.inputMode == 'input' ? "white" : "#FE3284"} size={28} name="chevron-back" />
          </TouchableOpacity>
          <Text style={styles.titleBar}>
            {state.inputMode == 'list' ? 'Contact' : 'Add Contact'}
          </Text>
          <TouchableOpacity onPress={() => state.setInputMode("input")}>
            <Icon color={state.inputMode == 'list' ? "white" : "#FE3284"} size={24} name="person-add" />
          </TouchableOpacity>
        </View>
        {state.inputMode != 'list' ? <InputView /> : <ListView />}
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  appBar: {
    height: 54,
    backgroundColor: "#FE3284",
    paddingHorizontal: 18,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    elevation: 4
  },
  titleBar: {
    fontSize: 22,
    color: "white",
    fontWeight: "bold"
  },
  titleText: {
    marginTop: 18,
    marginHorizontal: 18,
    marginBottom: 6,
    fontSize: 16
  },
  inputColumn: {
    paddingTop: 8,
    paddingHorizontal: 18,
  },
  textInput: {
    marginTop: 10,
    paddingHorizontal: 16,
    paddingVertical: 6,
    borderWidth: 1,
    borderColor: "rgba(0, 0, 0, 0.6)",
    borderRadius: 6
  },
  btnAdd: {
    backgroundColor: "#FE326F",
    borderRadius: 6,
    textAlign: 'center',
    marginTop: 10,
    padding: 12,
    fontWeight: 'bold',
    color: "white",
    fontSize: 18
  },
  listTile: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1.5,
    borderBottomColor: "#dedede",
    paddingHorizontal: 18,
    paddingVertical: 10
  },
  listTitle: {
    fontSize: 18,
    color: "rgba(0, 0, 0, 0.7)"
  },
  listSubtitle: {
    fontSize: 16,
    color: "rgba(0, 0, 0, 0.6)"
  },
})